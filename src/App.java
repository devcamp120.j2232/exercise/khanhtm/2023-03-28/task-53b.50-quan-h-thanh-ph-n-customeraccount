import com.devcamp.models.Account;
import com.devcamp.models.Customer;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1,"Khanh Tran",20);
        System.out.println("Customer1:");
        System.out.println(customer1);

        Customer customer2 = new Customer(2,"Hoàng hậu",30);
        System.out.println("Customer2:");
        System.out.println(customer2);

        Account account1 = new Account(3,customer1);
        System.out.println("Account1:");
        System.out.println(account1);

        Account account2 = new Account(4,customer2,4000000);
        System.out.println("Account2:");
        System.out.println(account2);

    }
}
